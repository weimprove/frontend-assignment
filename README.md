# Frontend assignment
An assignment to be solved when applying for a frontend developer job/internship at Improving.

## The assignment

Implement a responsive web design solution based on the design located in the design folder. The custom fonts needed are located in the fonts folder and on google fonts.

You define the breakpoints for the desktop and mobile view based on the design.

The product list should be implemented as a slider.

We encourage you to implement the design with either SASS or LESS, but it is not required.


## Requirements
Sketch - A free trial can be downloaded at https://www.sketchapp.com/
Photoshop - For windows users only, since Sketch is not available!

## Setup
Clone the project to your computer or make a fork here at Bitbucket.
```
git clone https://bitbucket.org/weimprove/frontend-assignment.git
```

## Apply to the job!
We look forward to your application. Send us a link to your solution (including source files).